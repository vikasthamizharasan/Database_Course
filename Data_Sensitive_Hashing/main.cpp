/*
 * Copyright (c) 2016 Vikas T
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "statisticsmodule.h"
#include "SHindex.h"
#include "SHgeneral.h"
#include "SHselection.h"
#include "data.h"
#include <iostream>

SHGeneral shg;
SHSelection shs;
SHIndex shi;
StatisticsModule st;

float data[datasize][D];

IO io;
Knn knn;

int main()
{
    io.diskread_float("datads.dat", data[0], datasize*D);
    cout << "data read from disk" << endl;

    // Generate a randomized query and the groundtruth by a linear scan.
    st.gen_query_and_groundtruth("query.dat","groundtruth.dat");

    // Create the index and store to disk
    shg.init();
    shs.radius_selection("decision.dat");
    shi.index_construct("decision.dat");
    shi.index_write("index.dat");
    shi.index_load("index.dat");

    // We use different number of hashtables and check accuracy
    int Ltemp[] = {5, 8, 10, 12, 15, 17, 20, 23, 26, 30, 35, 40, 45, 50};
    // for(int Lused = 5; Lused <= 50; ++Lused)
    for(int Lused : Ltemp)
    {
        // Load the query to memory
        shi.query_load("query.dat");

        // time the query
        st.begin();
        shi.query_execute(Lused);
        st.finish();

        // write out the results to disk
        shi.result_write("result.dat");

        // Write out the statistics
        st.stat_output("query.dat", "groundtruth.dat", "result.dat", "stats.txt", Lused);
    }

    cout << "program finished" << endl;
    return 0;
}

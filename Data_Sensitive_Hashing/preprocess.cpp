/*
 * Copyright (c) 2016 Vikas T
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 
#include "constants.h"
#include "io.h"
#define DATASOURCE "tests/input.data"
#define DATATARGET "datads3.dat"
#define QUERYSOURCE "tests/exp_query.txt"
#define QUERYTARGET "query3.dat"

#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>

float data[datasize][D];
float query[querysize][D];

IO io;

void create_datafile() {
    std::ifstream ifile(DATASOURCE);

    // Load the points from the file and dump to data.dat
    long long lineCount = 0;
    for (std::string line; std::getline(ifile, line) && lineCount < datasize; ++lineCount) {
        std::istringstream lineStream(line);

        for (int i = 0; i < D; ++i) {
            lineStream >> data[lineCount][i];
        }
    }

    ifile.close();

    // Write to disk
    io.diskwrite_float(DATATARGET, data[0], datasize * D);
}

void create_queryfile() {
    std::ifstream ifile(QUERYSOURCE);

    // Load the points from the file and dump to data.dat
    long long lineCount = 0;

    // Temp variables to read unwanted info
    int temp;

    for (std::string line; std::getline(ifile, line) && lineCount < querysize; ++lineCount) {
        std::istringstream lineStream(line);

        // Read the query number
        lineStream >> temp;

        // Read the query point
        for (int i = 0; i < D; ++i) {
            lineStream >> query[lineCount][i];
        }

        // Read the value of k
        lineStream >> temp;
    }

    ifile.close();

    // Write to disk
    io.diskwrite_float(QUERYTARGET, query[0], querysize * D);
}

int main() {
    create_datafile();
    // create_queryfile();
    return 0;
}

/*
 * Copyright (c) 2016 Vikas T
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 
#include "myrandom.h"
#include <iostream>
#include <random>
using namespace std;

/**
 * Generate a random num from 0 to max-1 (max may be any unsigned int)
 */
int MyRandom::int_random(int max)
{
    long long int longresult;
    int i = 1;
    longresult = rand();
    while (longresult < 16 * max)
    {
        i = 16 * i;
        longresult = 16 * longresult + rand();
    }
    int result = longresult%max;
    return result;
}

/**
 * Generate a simple multi gaussian distribution n dimensions variance matrix is variance * I
 * @param variance          variance
 */
void MyRandom::rand_multi_gaussian(float array[], int n, float variance)
{
    for (int i = 0; i < n; i++) array[i] = rand_single_gaussian(variance);
}

/**
 * Generate a random variable follows Gaussian distribution where mean = 0 varaince = variance
 * @param variance      The required variance
 */
float MyRandom::rand_single_gaussian(float variance)
{
    std::random_device rd;
    std::default_random_engine generator;
    generator.seed( rd() );
    std::normal_distribution<double> distribution(0, variance);
    return distribution(generator);
}

/*
 * Copyright (c) 2016 Vikas T
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 
#include "knn.h"
#include "myvector.h"
#include "constants.h"
#include <iostream>
#include <cmath>

using namespace std;

void Knn::init(){
    for (int i = 0; i <= K; i++)
    {
        knnlist[i] = -1;
        distlist[i] = -1;
    }
    sqrtbound = datasize;
}

/**
 * linear scan the dataset and put the result in knnlist (order is not maintained)
 */
void Knn::linear_scan(float data[][D], float querypoint[])
{
    init();
    for (int i = 0; i < datasize; i++)
    {
        addvertex(data, i, querypoint);
    }
}

/**
 * Check if data forcheck is a knn of querypoint, if yes maintain a new knn list
 */
void Knn::addvertex(float data[][D], int forcheck, float querypoint[])
{
    float dist;
    dist = MyVector::distancel2sq(D,data[forcheck], querypoint,0);
    if (knnlist[K - 1] == -1)
    {
        for (int i = 0; i < K; i++)
        {
            if (knnlist[i] == -1)
            {
                knnlist[i] = forcheck;// forcheck here is a label for the datapoint
                distlist[i] = dist;
                if (i == K - 1) computebound();
                return;
            }
        }
    }
    if (dist >= bound) return;
    knnlist[tochange] = forcheck;
    distlist[tochange] = dist;
    computebound();
}

/**
 * compute the bound : current largest knn distance and its index: tochange
 */
void Knn::computebound()
{
    bound = distlist[0];
    tochange = 0;
    for (int i = 1; i < K; i++)
    {
        if (distlist[i] > bound)
        {
            bound = distlist[i];
            tochange = i;
        }
    }
    sqrtbound = sqrt(bound);
}

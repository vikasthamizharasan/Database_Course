/*
 * Copyright (c) 2016 Vikas T
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 
#include "io.h"
#include <iostream>
#include <fstream>

using namespace std;

/**
 * Read an array of float from file
 * @param filename
 * @param array
 * @param size
 */
void IO::diskread_float(string filename, float array[], int size)
{
    FILE *fp;
    fp = fopen(filename.c_str(),"rb");
    if(fp == NULL)
    {
        cout << "Cannot open file!" << endl;;
        exit(1);
    }
    fread(array, sizeof(float), size, fp);
    fclose(fp);
}

/**
 * Write an array of float to file
 * @param filename
 * @param array
 * @param size
 */
void IO::diskwrite_float(string filename, float array[], int size)
{
    FILE *fp;
    fp = fopen(filename.c_str(),"wb");
    if(fp == NULL)
    {
        cout << "Cannot open file!" << endl;;
        exit(1);
    }
    fwrite(array, sizeof(float), size, fp);
    fclose(fp);
}

/**
 * Read an array of int from file
 * @param filename
 * @param array
 * @param size
 */
void IO::diskread_int(string filename, int array[], int size)
{
    FILE *fp;
    fp = fopen(filename.c_str(),"rb");
    if(fp == NULL)
    {
        cout << "Cannot open file!" << endl;;
        exit(1);
    }
    fread(array, sizeof(int), size, fp);
    fclose(fp);
}

/**
 * Write an array of int to file
 * @param filename
 * @param array
 * @param size
 */
void IO::diskwrite_int(string filename, int array[], int size)
{
    FILE *fp;
    fp = fopen(filename.c_str(),"wb");
    if(fp == NULL)
    {
        cout << "Cannot open file!" << endl;;
        exit(1);
    }
    fwrite(array, sizeof(int), size, fp);
    fclose(fp);
}

/*
 * Copyright (c) 2016 Vikas T
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 
#include "myvector.h"
#include <iostream>

using namespace std;

/**
 * Compute the sum of two vectors
 * @param dim      The number of dimensions
 * @param id1      First point
 * @param id2      Second point
 * @param result   Result point
 */
void MyVector::sum(int dim, float id1[], float id2[], float result[])
{
    for (int i = 0; i < dim; i++)
    {
        result[i] = (id1[i] - id2[i]) + (id1[i] - id2[i]);
    }
}

/**
 * Compute the dot product of two points
 * @param dim      The number of dimensions
 * @param id1      First point
 * @param id2      Second point
 * @return dotproduct
 */
float MyVector::dotproduct(int dim, float id1[], float id2[])
{
    float result = 0;
    for (int i = 0; i < dim; i++)
    {
        result += id1[i] * id2[i];
    }
    return result;
}

/**
 * Compute the L2 norm distance between two points
 * @param dim      The number of dimensions
 * @param id1      First point
 * @param id2      Second point
 * @param bound    FIXME: Nothing in this version
 * @return L2 norm
 */
float MyVector::distancel2sq(int dim, float id1[], float id2[], float bound)
{
    // to be optimized: use bound to filt
    float result = 0;
    for (int i = 0; i < dim; i++)
    {
        result += (id1[i] - id2[i]) * (id1[i] - id2[i]);
    }
    if(result < 0) cout<<"error negative distance" <<endl;
    return result;
}
